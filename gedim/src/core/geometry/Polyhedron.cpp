#include "Polyhedron.hpp"
#include "DefineNumbers.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"

namespace GeDiM
{

	Polyhedron::Polyhedron()
	{
		barycenter = NULL;
		centroid = NULL;
		measure = 0.0;
		squaredRadius = 0.0;
		radius = 0.0;
		aspectRatio = 0.0;
	}

	Polyhedron::~Polyhedron()
	{
		if(centroid != NULL)
			delete centroid;

		if(barycenter != NULL)
			delete barycenter;

		vertices.clear();
		segments.clear();
		faces.clear();
	}

	IPolyhedron* Polyhedron::Clone() const {
	    auto * poliedro = new Polyhedron;
	    Vertex * temp_vert;
	    Segment * temp_seg;
	    Polygon * temp_face;
	    for (unsigned int iter_vert = 0; iter_vert < NumberOfVertices(); iter_vert++) {
	        temp_vert = new Vertex(GetVertex(iter_vert)[0], GetVertex(iter_vert)[1], GetVertex(iter_vert)[2]);
	        poliedro->AddVertex(*temp_vert);
	    }
	    for (unsigned int iter_seg = 0; iter_seg < NumberOfSegments(); iter_seg++) {
	        temp_seg = new Segment;
            for (unsigned int iter_vert = 0; iter_vert < NumberOfVertices(); iter_vert++) {
                if (GetSegment(iter_seg).Origin()[0] == poliedro->GetVertex(iter_vert)[0] and
                        GetSegment(iter_seg).Origin()[1] == poliedro->GetVertex(iter_vert)[1] and
                        GetSegment(iter_seg).Origin()[2] == poliedro->GetVertex(iter_vert)[2]) {
                    temp_seg->AddVertex(poliedro->GetVertex(iter_vert));
                  }
                else if (GetSegment(iter_seg).End()[0] == poliedro->GetVertex(iter_vert)[0] and
                    GetSegment(iter_seg).End()[1] == poliedro->GetVertex(iter_vert)[1] and
                    GetSegment(iter_seg).End()[2] == poliedro->GetVertex(iter_vert)[2]) {
                    temp_seg->AddVertex(poliedro->GetVertex(iter_vert));
                }
            }
            poliedro->AddSegment(*temp_seg);
	    }
	    for (unsigned int iter_face = 0; iter_face < Faces().size(); iter_face++) {
	        const auto & anl_face = dynamic_cast<const Polygon &>(GetFace(iter_face));
	        temp_face = new Polygon;
	        for (unsigned int iter_vert = 0; iter_vert < anl_face.Vertices().size(); iter_vert++) {
	            const Vertex & anl_vert = anl_face.GetVertex(iter_vert);
	            for (unsigned int iter_vert_copy = 0; iter_vert_copy < NumberOfVertices(); iter_vert_copy++) {
	                const Vertex & anl_vert_copy = poliedro->GetVertex(iter_vert_copy);
	                if (anl_vert[0] == anl_vert_copy[0] and
	                    anl_vert[1] == anl_vert_copy[1] and
	                    anl_vert[2] == anl_vert_copy[2]) {
	                    temp_face->AddVertex(anl_vert_copy);
	                }
	            }
	        }
	        for (unsigned int iter_seg = 0; iter_seg < GetFace(iter_face).NumberOfSegments(); iter_seg++) {
	            const auto & anl_seg = dynamic_cast<const Segment &>(anl_face.GetSegment(iter_seg));
	            for (unsigned int iter_seg_copy = 0; iter_seg_copy < NumberOfSegments(); iter_seg_copy++) {
                    const auto & anl_seg_copy = dynamic_cast<const Segment &>(poliedro->GetSegment(iter_seg_copy));
                    if ((anl_seg.Origin()[0] == anl_seg_copy.Origin()[0] and
                            anl_seg.Origin()[1] == anl_seg_copy.Origin()[1] and
                            anl_seg.Origin()[2] == anl_seg_copy.Origin()[2] and
                            anl_seg.End()[0] == anl_seg_copy.End()[0] and
                            anl_seg.End()[1] == anl_seg_copy.End()[1] and
                            anl_seg.End()[2] == anl_seg_copy.End()[2]) or
                            (anl_seg.Origin()[0] == anl_seg_copy.End()[0] and
                             anl_seg.Origin()[1] == anl_seg_copy.End()[1] and
                             anl_seg.Origin()[2] == anl_seg_copy.End()[2] and
                             anl_seg.End()[0] == anl_seg_copy.Origin()[0] and
                             anl_seg.End()[1] == anl_seg_copy.Origin()[1] and
                             anl_seg.End()[2] == anl_seg_copy.Origin()[2])) {
                        temp_face->AddSegment(anl_seg_copy);
                    }
	            }
	        }
	        poliedro->AddFace(*temp_face);
	    }

        return poliedro;
	}


}