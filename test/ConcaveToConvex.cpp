#include <stack>
#include <unordered_map>
#include <unordered_set>
#include "Polygon.hpp"
#include "Polyhedron.hpp"
#include "ConcaveToConvex.hpp"
#include "Intersector2D1D.hpp"


using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  bool ConcaveToConvex::IsPolygonConvex(const IPolygon& polygon, const double& tolerance)
  {
    return false;
  }
  // ***************************************************************************
  Output::ExitCodes ConcaveToConvex::ConcaveToConvexPolygon(const IPolygon& concavePolygon, list<IPolygon*>& convexPolygons, const double& tolerance)
  {
    convexPolygons.push_back(concavePolygon.Clone());

    return Output::UnimplementedMethod;
  }
  // ***************************************************************************
  bool ConcaveToConvex::IsPolyhedronConvex(const IPolyhedron& polyhedron, const double& tolerance) {
      std::ofstream SaveFile("output_poliedro.txt", std::ios_base::out | std::ios_base::app);
      // inizializzo il poliedro
      const auto & poliedro = dynamic_cast<const Polyhedron &>(polyhedron);
      Polygon anl_face;
      Vector3d normal;
      // verrà aperto in sola scrittura ed eventuali contenuti, se presenti, saranno sovrascritti
      if (SaveFile.is_open()){

          // stampo tutti gli attributi di un poliedro: vertici, segmenti, facce

          //conto vertici, lati e facce
          SaveFile << "Nel poliedro ci sono " << poliedro.NumberOfVertices() << " vertici\n";
          SaveFile << "Nel poliedro ci sono " << poliedro.NumberOfSegments() << " lati\n";
          SaveFile << "Nel poliedro ci sono " << poliedro.NumberOfFaces() << " facce\n";

          //stampo tutti i vertici
          for (unsigned int i = 0; i < poliedro.NumberOfVertices(); i++){
              SaveFile << "Il vertice " << i << " ha coordinate " << poliedro.GetVertex(i)[0] << ", " << poliedro.GetVertex(i)[1] << ", " << poliedro.GetVertex(i)[2] << "\n";
          }
          SaveFile << '\n';

          //stampo tutti i lati
          for (unsigned int i = 0; i < poliedro.NumberOfSegments(); i++){
              SaveFile << "Il segmento " << i << " ha come origine " << poliedro.GetSegment(i).Origin()[0] << ", " << poliedro.GetSegment(i).Origin()[1] << ", " << poliedro.GetSegment(i).Origin()[2];
              SaveFile << ", il segmento " << i << " ha come fine " << poliedro.GetSegment(i).End()[0] << ", " << poliedro.GetSegment(i).End()[1] << ", " << poliedro.GetSegment(i).End()[2] << '\n';
          }
          SaveFile << '\n' << '\n';


          // stampo tutte le facce
          for (unsigned int i = 0; i < poliedro.NumberOfFaces(); i++){
              const auto & temp_face = dynamic_cast<const Polygon &>(poliedro.GetFace(i));
              SaveFile << "La faccia " << i << " ha " << temp_face.NumberOfVertices() << " vertici e " << temp_face.NumberOfSegments() << " lati\n";
              //Ciclo sui vertici della faccia
              for (unsigned int j= 0; j < temp_face.NumberOfVertices(); j++){
                  SaveFile << "Il vertice " << j << " ha coordinate " << temp_face.GetVertex(j)[0] << ", " << temp_face.GetVertex(j)[1] << ", " << temp_face.GetVertex(j)[2] << "\n";
              }
              //Ciclo sui lati della faccia
              for (unsigned int k = 0; k < temp_face.NumberOfSegments(); k++){
                  const Vertex origine = temp_face.GetSegment(k).Origin();
                  const Vertex fine = temp_face.GetSegment(k).End();
                  SaveFile << "Il segmento " << k << " ha come origine " << origine[0] << ", " << origine[1] << ", " << origine[2] << ' ';
                  SaveFile << ", il segmento " << k << " ha come fine " << fine[0] << ", " << fine[1] << ", " << fine[2] << '\n';
              }
              SaveFile << '\n';
          }
      }
      SaveFile << "/*****************************************************************************\n";
      SaveFile.close();

      for (unsigned int iter_face = 0; iter_face < poliedro.NumberOfFaces(); iter_face++) {
          //Si effettuo il casting per la faccia in modo da renderla 'utilizzabile' e si estrae la sua normale
          const auto &temp_face = dynamic_cast<const Polygon &>(poliedro.GetFace(iter_face));
          anl_face = const_cast<Polygon &>(temp_face);
          anl_face.ComputePlane();
          normal = anl_face.Normal();
          double scal_prod = 0;
          for (unsigned int cmprd_vertex = 0; cmprd_vertex < poliedro.NumberOfVertices(); cmprd_vertex++) {
              Vector3d segm_anl = poliedro.GetVertex(cmprd_vertex) - poliedro.GetFace(iter_face).GetVertex(0);
              scal_prod = segm_anl.dot((-1)*normal);
              // Aggiornamento mappa dei vertici con i colori riferiti alla faccia analizzata
              if  (scal_prod < -tolerance) {
                  return Output::GenericError;
              }
          }
      }
    return Output::Success;
  }
  // ***************************************************************************
  bool check_vertexes(const IPolyhedron &, const Vertex &, Vector3d &,
                      std::unordered_map<const Vertex *, char> &,
                      const double &);
  // ***************************************************************************
  void add_unambiguous_vertexes(const IPolyhedron &, Polyhedron &, Polyhedron &,
                                std::unordered_map<const Vertex *, char> &);
  // ***************************************************************************
  void manage_segments(const IPolyhedron &, Polyhedron &, Polyhedron &,
                       std::unordered_map<const Vertex *, char> &,
                       std::unordered_map<const ISegment *, Vertex *> &,
                       std::vector<const ISegment *> &);
  // ***************************************************************************
  void create_cut_face(std::vector<const ISegment *> &, std::vector<Polygon *> &,
                       std::vector<Polygon *> &, Vector3d &);
  // ***************************************************************************
  void assign_face(std::vector<Polygon *> &, Polyhedron &);
  // ***************************************************************************
  Output::ExitCodes ConcaveToConvex::ConcaveToConvexPolyhedron(const IPolyhedron& concavePolyhedron, list<IPolyhedron*>& convexPolyhedra, const double& tolerance) {
      std::stack<const IPolyhedron *> family;
      family.push(&concavePolyhedron);
      std::unordered_map<const Vertex *, char> vertexes;
      const IPolyhedron *anl_poly;
      Polygon anl_face;
      Vector3d normal;
      bool found = false;

      //Ciclo sugli elementi dello stack, che si interrompe nel momento in cui questo è vuoto, ovvero quando ogni
      //poliedro e sotto-poliedro è stato diviso in oggetti convessi
      while (!family.empty()) {
          found = false;
          anl_poly = family.top();
          //Ciclo sulle facce del poliedri estratto. Si interrompe nel momento in cui il test è stato fatto su tutte le
          //facce oppure quando è stata trovata la faccia lungo cui tagliare
          for (unsigned int iter_face = 0; iter_face < anl_poly->NumberOfFaces() and not found; iter_face++) {
              //Si effettuo il casting per la faccia in modo da renderla 'utilizzabile' e si estrae la sua normale
              const auto &temp_face = dynamic_cast<const Polygon &>(anl_poly->GetFace(iter_face));
              anl_face = const_cast<Polygon &>(temp_face);
              anl_face.ComputePlane();
              normal = anl_face.Normal();
              vertexes.clear();

              // Inizializzazione mappa vertici. Per ogni vertice della faccia considerata si studia il suo 'colore'
              // e lo si aggiunge alla mappa dei vertici. Se uno di loro è rosso, allora si attiva la flag e la faccia
              // corrente diventa quella lungo cui tagliare
              found = check_vertexes(*anl_poly, anl_face.GetVertex(0), normal, vertexes, tolerance);
          }
          if (found) {
              // Trovata la faccia lungo cui tagliare, vengono dichiarati i figli e si procede con la loro costruzione
              Polyhedron *blue_child = new Polyhedron;
              Polyhedron *red_child = new Polyhedron;
              unsigned int b_count = 0;
              unsigned int r_count = 0;
              unsigned int w_count = 0;
              double scal_prod;
              std::vector<const ISegment *> white_segments;
              std::unordered_map<const ISegment *, Vertex *> mixed_segments;
              std::vector<Polygon *> blue_white_faces;
              std::vector<Polygon *> red_white_faces;
              Polygon * blue;
              Polygon * red;
              Segment * seg_1;
              Segment * seg_2;
              Segment * temp_seg;
              Vertex * temp_vertex;
              Intersector2D1D * intersezione;
              char prec;
              char color;

              // Inizia la ricostruzione dei poliedri figli. Verranno prima aggiunti tutti gli elementi del poliedro
              // attribuibili senza ambiguità al figlio blu o quello rosso

              // Questo ciclo aggiunge tutti i vertici blu o rossi, ma non quelli bianchi
              add_unambiguous_vertexes(*anl_poly, *blue_child, *red_child, vertexes);

              // Questo ciclo aggiunge tutti gli spigoli rossi, blu, bianco-rossi e bianco-blu, ma non quelli
              // bianchi-bianchi o rosso-blu. Tuttavia ricorda i segmenti bianchi-bianchi in un vettore e quelli
              // rosso-blu in una mappa
              manage_segments(*anl_poly, *blue_child, *red_child, vertexes, mixed_segments, white_segments);

              //Aggiunge le facce blu e quelle rosse, distinguendo anche quelle bianche in blu-bianche e rosso-bianche
              for (unsigned int iter_face = 0; iter_face < 6; iter_face++) {
                  b_count = 0;
                  r_count = 0;
                  w_count = 0;

                  //Una per volta, si 'casta' ogni faccia da un IPolygon ad un Polygon per poter estrarre i vertici
                  const Polygon &cmprd_face = dynamic_cast<const Polygon &>(anl_poly->GetFace(iter_face));

                  // Per ogni vertice si guarda il colore e si aggiornano i rispettivi count
                  for (unsigned int iter_vert = 0; iter_vert < cmprd_face.NumberOfVertices(); iter_vert++) {
                      const char &cmprd_vert = vertexes[&cmprd_face.GetVertex(iter_vert)];
                      if (cmprd_vert == 'b' or cmprd_vert == 'w')
                          b_count++;
                      if (cmprd_vert == 'r' or cmprd_vert == 'w')
                          r_count++;
                      if (cmprd_vert == 'w')
                          w_count++;
                  }

                  //Se un count è uguale al numero massimo dei lati, la faccia appartiene al poliedro corrispettivo
                  // al count. Per le facce complanari a quella di taglio e la faccia stessa si procede a calcolare la
                  // normale per poi inserirle nella lista corretta
                  if (b_count == cmprd_face.NumberOfVertices() and w_count != cmprd_face.NumberOfVertices()) {
                      blue_child->AddFace(cmprd_face);
                  }
                  else if (r_count == cmprd_face.NumberOfVertices() and w_count != cmprd_face.NumberOfVertices()) {
                      red_child->AddFace(cmprd_face);
                  }
                  else if (w_count == cmprd_face.NumberOfVertices()) {
                      Polygon &temp_face = const_cast<Polygon &>(cmprd_face);
                      temp_face.ComputePlane();
                      scal_prod = temp_face.Normal().dot(normal);
                      if (scal_prod > 0)
                          blue_white_faces.push_back(&temp_face);
                      else
                          red_white_faces.push_back(&temp_face);
                  }
                  // Se nessuna delle condizioni è raggiunta, è necessario tagliare la faccia in due sottofacce
                  else {
                      blue = new Polygon;
                      red = new Polygon;
                      temp_seg = new Segment;
                      //Quando si incontra un vertice bianco lo si aggiunge al nuovo segmento di taglio,
                      // se è il secondo vertice bianco si aggiunge il nuovo segmento alle facce
                      for (unsigned int iter_vertex = 0; iter_vertex <= cmprd_face.Vertices().size(); iter_vertex++) {
                          color = vertexes[&cmprd_face.GetVertex(iter_vertex % cmprd_face.Vertices().size())];

                          // Per il primo vertice considerato, basta aggiungere solo il vertice alla faccia corretta,
                          // senza aggiungere nessuno spigolo
                          if (iter_vertex == 0) {
                              if (color == 'w') {
                                  temp_seg->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                              }
                              else if (color == 'b') {
                                  blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                              }
                              else {
                                  red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                              }
                          }

                          // Quando il ciclo torna al primo vertice, per chiudere le facce serve aggiungere l'ultimo
                          // segmento ma nessun altro vertice
                          else if (iter_vertex == cmprd_face.Vertices().size()) {
                              if (color == prec) {
                                  if (color == 'b') {
                                      blue->AddSegment(cmprd_face.GetSegment(iter_vertex - 1));
                                  }
                                  else {
                                      red->AddSegment(cmprd_face.GetSegment(iter_vertex - 1));
                                  }
                              }
                              else if (prec == 'w') {
                                  if (color == 'b') {
                                      blue->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  }
                                  else {
                                      red->AddSegment(cmprd_face.GetSegment(iter_vertex - 1));
                                  }
                              }
                              else if (prec == 'b' and color == 'w') {
                                  blue->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                              }
                              else if (prec == 'r' and color == 'w') {
                                  red->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                              }
                              else {
                                  seg_1 = new Segment();
                                  seg_2 = new Segment();
                                  intersezione = new Intersector2D1D;
                                  intersezione->SetToleranceIntersection(tolerance);
                                  intersezione->SetToleranceParallelism(tolerance);
                                  intersezione->SetLine(cmprd_face.GetVertex(iter_vertex-1), (cmprd_face.GetVertex(0) - cmprd_face.GetVertex(iter_vertex-1)));
                                  intersezione->SetPlane(normal, anl_face.PlaneTranslation());
                                  intersezione->ComputeIntersection();
                                  if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] != nullptr) {
                                      temp_vertex = mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)];
                                  }
                                  else
                                      temp_vertex = new Vertex(intersezione->IntersectionPoint()[0], intersezione->IntersectionPoint()[1], intersezione->IntersectionPoint()[2]);
                                  seg_1->Initialize(cmprd_face.GetVertex(iter_vertex-1), *temp_vertex);
                                  seg_2->Initialize(*temp_vertex, cmprd_face.GetVertex(0));
                                  temp_seg->AddVertex(*temp_vertex);
                                  white_segments.push_back(temp_seg);
                                  if (prec == 'b') {
                                      if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] == nullptr) {
                                          blue_child->AddSegment(*seg_1);
                                          red_child->AddSegment(*seg_2);
                                          mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] = temp_vertex;
                                      }
                                      blue->AddVertex(*temp_vertex);
                                      blue->AddSegment(*seg_1);
                                      red->AddVertex(*temp_vertex);
                                      blue->AddSegment(*temp_seg);
                                      red->AddSegment(*temp_seg);
                                      red->AddSegment(*seg_2);
                                  }
                                  else {
                                      if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] == nullptr) {
                                          red_child->AddSegment(*seg_1);
                                          blue_child->AddSegment(*seg_2);
                                          mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] = temp_vertex;
                                      }
                                      red->AddVertex(*temp_vertex);
                                      red->AddSegment(*seg_1);
                                      blue->AddVertex(*temp_vertex);
                                      red->AddSegment(*temp_seg);
                                      blue->AddSegment(*temp_seg);
                                      blue->AddSegment(*seg_2);
                                  }
                                  delete intersezione;
                              }
                          }

                          //In tutti gli altri casi si aggiunge il vertice considerato e lo spigolo precedente alla
                          // faccia corretta
                          else {
                              if (color == prec) {
                                  if (color == 'b') {
                                      blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                      blue->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  }
                                  else {
                                      red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                      red->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  }
                              }
                              else if (prec == 'w') {
                                  if (color == 'b') {
                                      blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                      blue->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  }
                                  else {
                                      red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                      red->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  }
                              }
                              else if (prec == 'b' and color == 'w') {
                                  blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  blue->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  temp_seg->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  if (temp_seg->NumberOfVertices() > 1) {
                                      white_segments.push_back(temp_seg);
                                      blue->AddSegment(*temp_seg);
                                      red->AddSegment(*temp_seg);
                                  }
                              }
                              else if (prec == 'r' and color == 'w') {
                                  red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  red->AddSegment(cmprd_face.GetSegment(iter_vertex-1));
                                  blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  temp_seg->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                  if (temp_seg->NumberOfVertices() > 2) {
                                      white_segments.push_back(temp_seg);
                                      red->AddSegment(*temp_seg);
                                      blue->AddSegment(*temp_seg);
                                  }
                              }
                              else {
                                  seg_1 = new Segment();
                                  seg_2 = new Segment();
                                  intersezione = new Intersector2D1D;
                                  intersezione->SetToleranceIntersection(tolerance);
                                  intersezione->SetToleranceParallelism(tolerance);
                                  intersezione->SetLine(cmprd_face.GetVertex(iter_vertex-1), (cmprd_face.GetVertex(iter_vertex) - cmprd_face.GetVertex(iter_vertex-1)));
                                  intersezione->SetPlane(normal, anl_face.PlaneTranslation());
                                  intersezione->ComputeIntersection();
                                  if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] != nullptr) {
                                      temp_vertex = mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)];
                                  }
                                  else
                                      temp_vertex = new Vertex(intersezione->IntersectionPoint()[0], intersezione->IntersectionPoint()[1], intersezione->IntersectionPoint()[2]);
                                  seg_1->Initialize(cmprd_face.GetVertex(iter_vertex-1), *temp_vertex);
                                  seg_2->Initialize(*temp_vertex, cmprd_face.GetVertex(iter_vertex));
                                  temp_seg->AddVertex(*temp_vertex);
                                  if (prec == 'b') {
                                      blue->AddVertex(*temp_vertex);
                                      blue->AddSegment(*seg_1);
                                      red->AddVertex(*temp_vertex);
                                      if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] == nullptr) {
                                          blue_child->AddSegment(*seg_1);
                                          red_child->AddSegment(*seg_2);
                                          mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] = temp_vertex;
                                      }
                                      if (temp_seg->NumberOfVertices() == 1) {
                                          red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                          red->AddSegment(*seg_2);
                                      }
                                      else {
                                          white_segments.push_back(temp_seg);
                                          blue->AddSegment(*temp_seg);
                                          red->AddSegment(*temp_seg);
                                          red->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                          red->AddSegment(*seg_2);
                                      }
                                  }
                                  else {
                                      red->AddVertex(*temp_vertex);
                                      red->AddSegment(*seg_1);
                                      blue->AddVertex(*temp_vertex);
                                      if (mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] == nullptr) {
                                          red_child->AddSegment(*seg_1);
                                          blue_child->AddSegment(*seg_2);
                                          mixed_segments[&cmprd_face.GetSegment(iter_vertex-1)] = temp_vertex;
                                      }
                                      if (temp_seg->NumberOfVertices() == 1) {
                                          blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                          blue->AddSegment(*seg_2);
                                      }
                                      else {
                                          white_segments.push_back(temp_seg);
                                          red->AddSegment(*temp_seg);
                                          blue->AddSegment(*temp_seg);
                                          blue->AddVertex(cmprd_face.GetVertex(iter_vertex));
                                          blue->AddSegment(*seg_2);
                                      }
                                  }
                                  delete intersezione;
                              }
                          }
                          prec = color;
                      }
                      blue_child->AddFace(*blue);
                      red_child->AddFace(*red);
                  }
              }
              create_cut_face(white_segments, red_white_faces, blue_white_faces, normal);
              white_segments.clear();
              assign_face(blue_white_faces, *blue_child);
              assign_face(red_white_faces, *red_child);
              family.pop();
              family.push(blue_child);
              family.push(red_child);
          }
          else {
              // Se non è stata trovata la faccia lungo cui tagliare, il poliedro è convesso e può essere aggiunto alla
              // lista dei sotto-poliedri convessi derivati dal poliedro concavo originale
              convexPolyhedra.push_back(anl_poly->Clone());
              family.pop();
          }
      }


          return Output::Success;
  }
  // ***************************************************************************
  bool check_vertexes(const IPolyhedron & anl_poly, const Vertex & vertice, Vector3d & normal,
                      std::unordered_map<const Vertex *, char> & vertexes, const double & tolerance) {
      bool found = false;
      double scal_prod = 0;
      for (unsigned int cmprd_vertex = 0; cmprd_vertex < anl_poly.NumberOfVertices(); cmprd_vertex++) {
          //Si inizializza il segmento tra un vertice della faccia analizzata e l'altro vertice del poliedro,
          // in modo da poter calcolare il prodotto scalare tra questo e la normale alla faccia analizzata
          Vector3d segm_anl = anl_poly.GetVertex(cmprd_vertex) - vertice;
          scal_prod = segm_anl.dot((-1)*normal);
          // Aggiornamento mappa dei vertici con i colori riferiti alla faccia analizzata
          if (scal_prod > tolerance)
              vertexes[&anl_poly.GetVertex(cmprd_vertex)] = 'b';
          else if  (scal_prod < -tolerance) {
              vertexes[&anl_poly.GetVertex(cmprd_vertex)] = 'r';
              found = true;
          }
          else
              vertexes[&anl_poly.GetVertex(cmprd_vertex)] = 'w';
      }
      return found;
  }
  // ***************************************************************************
  void add_unambiguous_vertexes(const IPolyhedron & anl_poly, Polyhedron & blue_child, Polyhedron & red_child,
                                std::unordered_map<const Vertex *, char> & vertexes) {
      for (unsigned int iter_vertex = 0; iter_vertex < anl_poly.Vertices().size(); iter_vertex++) {
          if (vertexes[&anl_poly.GetVertex(iter_vertex)] == 'b') {
              blue_child.AddVertex(anl_poly.GetVertex(iter_vertex));
          }
          else if (vertexes[&anl_poly.GetVertex(iter_vertex)] == 'r') {
              red_child.AddVertex(anl_poly.GetVertex(iter_vertex));
          }
      }

  }
  // ***************************************************************************
  void manage_segments(const IPolyhedron & anl_poly, Polyhedron & blue_child, Polyhedron & red_child,
                       std::unordered_map<const Vertex *, char> & vertexes,
                       std::unordered_map<const ISegment *, Vertex *> & mixed_segments,
                       std::vector<const ISegment *> & white_segments) {
      char org_color;
      char end_color;
      std::vector<const ISegment *> temp_white_segments;
      std::unordered_set<const Vertex *> cut_vert;
      for (unsigned int iter_segm = 0; iter_segm < anl_poly.Segments().size(); iter_segm++) {
          org_color = vertexes[&anl_poly.GetSegment(iter_segm).Origin()];
          end_color = vertexes[&anl_poly.GetSegment(iter_segm).End()];
          if (org_color == end_color) {
              if (org_color == 'b')
                  blue_child.AddSegment(anl_poly.GetSegment(iter_segm));
              else if (org_color == 'r')
                  red_child.AddSegment(anl_poly.GetSegment(iter_segm));
              else
                  temp_white_segments.push_back(&anl_poly.GetSegment(iter_segm));
          }
          else {
              if (org_color == 'b') {
                  if (end_color == 'w') {
                      blue_child.AddSegment(anl_poly.GetSegment(iter_segm));
                  }
                  else
                      mixed_segments[&anl_poly.GetSegment(iter_segm)] = nullptr;
              }
              else if (org_color == 'w') {
                  if (end_color == 'b') {
                      blue_child.AddSegment(anl_poly.GetSegment(iter_segm));
                  }
                  else if (end_color == 'r') {
                      red_child.AddSegment(anl_poly.GetSegment(iter_segm));
                      cut_vert.insert(&anl_poly.GetSegment(iter_segm).Origin());
                  }
              }
              else {
                  if (end_color == 'w') {
                      red_child.AddSegment(anl_poly.GetSegment(iter_segm));
                      cut_vert.insert(&anl_poly.GetSegment(iter_segm).End());
                  }
                  else if (end_color == 'b')
                      mixed_segments[&anl_poly.GetSegment(iter_segm)] = nullptr;
              }
          }
      }
      for (unsigned int iter_seg = 0; iter_seg < temp_white_segments.size(); iter_seg++) {
          if (cut_vert.count(&temp_white_segments[iter_seg]->Origin()) == 1 and cut_vert.count(&temp_white_segments[iter_seg]->End())) {
              white_segments.push_back(temp_white_segments[iter_seg]);
          }
      }
      temp_white_segments.clear();
      cut_vert.clear();
  }
  // ***************************************************************************
  void create_cut_face(std::vector<const ISegment *> & white_seg, std::vector<Polygon *> & red_faces,
                       std::vector<Polygon *> & blue_faces, Vector3d & normal) {
      int iter_seg;
      int iter;
      int size = white_seg.size();
      Polygon * new_face;
      Polygon * opposite_face;
      const ISegment * temp_seg;
      const Vertex * first;
      const Vertex * last;
      bool close;
      bool done = true;
      std::unordered_set<const Vertex *> vertex;
      for (unsigned int iter_face = 0; iter_face < red_faces.size(); iter_face++) {
          for (unsigned int iter_vert = 0; iter_vert < red_faces[iter_face]->NumberOfVertices(); iter_vert++) {
              vertex.insert(&red_faces[iter_face]->GetVertex(iter_vert));
          }
      }
      for (iter_seg = 0; iter_seg < size; iter_seg++) {
          if (vertex.count(&white_seg[iter_seg]->Origin()) == 1 or vertex.count(&white_seg[iter_seg]->End()) == 1) {
              white_seg[iter_seg] = nullptr;
          }
      }
      vertex.clear();
      while (done) {
          done = false;
          new_face = new Polygon;
          for (iter = 0; iter < white_seg.size() and !done; iter++) {
              if (white_seg[iter] != nullptr) {
                  done = true;
                  new_face->AddVertex(white_seg[iter]->Origin());
                  new_face->AddVertex(white_seg[iter]->End());
                  new_face->AddSegment(*white_seg[iter]);
              }
          }
          if (done) {
              temp_seg = white_seg[iter-1];
              white_seg[iter-1] = nullptr;
              first = &temp_seg->Origin();
              last = &temp_seg->End();
              close = false;
              for (iter = 0; !close; iter++) {
                  iter_seg = iter % size;
                  if (temp_seg != white_seg[iter_seg] and white_seg[iter_seg] != nullptr) {
                      const Vertex & origin = white_seg[iter_seg]->Origin();
                      const Vertex & end = white_seg[iter_seg]->End();
                      if ((first == &origin and last == &end) or (first == &end and last == &origin)){
                          close = true;
                          new_face->AddSegment(*white_seg[iter_seg]);
                          white_seg[iter_seg] = nullptr;
                      }
                      else if (last == &origin) {
                          new_face->AddVertex(end);
                          new_face->AddSegment(*white_seg[iter_seg]);
                          temp_seg = white_seg[iter_seg];
                          last = &end;
                          white_seg[iter_seg] = nullptr;
                      }
                      else if (last == &end) {
                          new_face->AddVertex(origin);
                          new_face->AddSegment(*white_seg[iter_seg]);
                          temp_seg = white_seg[iter_seg];
                          last = &origin;
                          white_seg[iter_seg] = nullptr;
                      }
                  }
              }
              new_face->ComputePlane();
              opposite_face = new Polygon;
              opposite_face->AddVertex(new_face->GetVertex(0));
              for (int iter_vert = new_face->NumberOfVertices()-1; iter_vert >0; iter_vert--) {
                  opposite_face->AddVertex(new_face->GetVertex(iter_vert));
              }
              for (iter_seg = new_face->NumberOfSegments()-1; iter_seg >= 0; iter_seg--) {
                  opposite_face->AddSegment(new_face->GetSegment(iter_seg));
              }
              if (normal.dot(new_face->Normal()) > 0) {
                  blue_faces.push_back(new_face);
                  red_faces.push_back(opposite_face);
              }
              else {
                  red_faces.push_back(new_face);
                  blue_faces.push_back(opposite_face);
              }
          }
          else {
              delete new_face;
          }
      }
  }
    // ***************************************************************************
  void assign_face(std::vector<Polygon *> & lista, Polyhedron & poliedro) {
      std::unordered_set<const Vertex *> vertexes;
      std::unordered_set<const ISegment *> segments;
      for (unsigned int iter_face = 0; iter_face < lista.size(); iter_face++) {
          for (unsigned int iter_vertex = 0; iter_vertex < lista[iter_face]->NumberOfVertices(); iter_vertex++) {
              const Vertex & anl_vertex = lista[iter_face]->GetVertex(iter_vertex);
              if (vertexes.count(&anl_vertex) == 0) {
                  vertexes.insert(&anl_vertex);
                  poliedro.AddVertex(anl_vertex);
              }
          }
          for (unsigned int iter_seg = 0; iter_seg < lista[iter_face]->NumberOfSegments(); iter_seg++) {
              const ISegment & anl_seg = lista[iter_face]->GetSegment(iter_seg);
              if (segments.count(&anl_seg) == 0) {
                  segments.insert(&anl_seg);
                  poliedro.AddSegment(anl_seg);
              }
          }
          poliedro.AddFace(*lista[iter_face]);
      }
      vertexes.clear();
      segments.clear();
  }
  // ***************************************************************************
}