unsigned int numVertices = 12, numSegments = 23, numFaces = 11;

          concavePolyhedron.Initialize(numVertices, numSegments, numFaces);

          vertices.reserve(numVertices);
          vertices.push_back(Vertex(-0.44e+00, -0.35e+00, 3.2e+00)); // 0
          vertices.push_back(Vertex(1.27406e+00, -0.35e+00, 3.2e+00)); // 1
          vertices.push_back(Vertex(-0.44e+00, -0.35e+00, 0e+00)); // 2
          vertices.push_back(Vertex(1.27406e+00, -0.35e+00, 0e+00)); // 3
          vertices.push_back(Vertex(-0.44e+00, 2.37e+00, 1.49e+00)); // 4
          vertices.push_back(Vertex(1.57264e+00, 2.37e+00, 1.49e+00)); // 5
          vertices.push_back(Vertex(-0.44e+00, 2.71e+00, 0e+00)); // 6
          vertices.push_back(Vertex(3.36542e+00, 0.54165e+00, 0e+00)); // 7
          vertices.push_back(Vertex(1.27406e+00, -0.35e+00, 1.49e+00)); // 8
          vertices.push_back(Vertex(3.59645e+00, 1.3646e+00, 1.49e+00)); // 9
          vertices.push_back(Vertex(4.32e+00, 2.71e+00, 0e+00)); // 10
          vertices.push_back(Vertex(4.32e+00, 2.37e+00, 1.49e+00)); // 11

          for (unsigned int s = 0; s < numVertices; s++)
              concavePolyhedron.AddVertex(vertices[s]);

          segments.reserve(numSegments);
          for (unsigned int v = 0; v < numSegments; v++)
              segments.push_back(new Segment(v+1));

          segments[0]->Initialize(vertices[0], vertices[1]);
          segments[1]->Initialize(vertices[1], vertices[3]);
          segments[2]->Initialize(vertices[2], vertices[0]);
          segments[3]->Initialize(vertices[3], vertices[2]);
          segments[4]->Initialize(vertices[1], vertices[5]);
          segments[5]->Initialize(vertices[4], vertices[0]);
          segments[6]->Initialize(vertices[5], vertices[4]);
          segments[7]->Initialize(vertices[8], vertices[5]);
          segments[8]->Initialize(vertices[1], vertices[8]);
          segments[9]->Initialize(vertices[10], vertices[11]);
          segments[10]->Initialize(vertices[11], vertices[7]);
          segments[11]->Initialize(vertices[7], vertices[10]);
          segments[12]->Initialize(vertices[8], vertices[9]);
          segments[13]->Initialize(vertices[9], vertices[7]);
          segments[14]->Initialize(vertices[7], vertices[8]);
          segments[15]->Initialize(vertices[3], vertices[8]);
          segments[16]->Initialize(vertices[4], vertices[11]);
          segments[17]->Initialize(vertices[6], vertices[4]);
          segments[18]->Initialize(vertices[10], vertices[6]);
          segments[19]->Initialize(vertices[5], vertices[11]);
          segments[20]->Initialize(vertices[11], vertices[9]);
          segments[21]->Initialize(vertices[2], vertices[6]);
          segments[22]->Initialize(vertices[7], vertices[3]);

          for (unsigned int s = 0; s < numSegments; s++)
              concavePolyhedron.AddSegment(*segments[s]);

          faces.reserve(numFaces);
          for (unsigned int f = 0; f < numFaces; f++)
          {
              faces.push_back(new Polygon(f + 1));
              IPolygon& face = *faces[f];

              face.Initialize(3, 3);
          }

          faces[0]->AddVertex(vertices[0]);
          faces[0]->AddVertex(vertices[2]);
          faces[0]->AddVertex(vertices[3]);
          faces[0]->AddVertex(vertices[1]);
          faces[0]->AddSegment(*segments[2]);
          faces[0]->AddSegment(*segments[3]);
          faces[0]->AddSegment(*segments[1]);
          faces[0]->AddSegment(*segments[0]);
          faces[1]->AddVertex(vertices[0]);
          faces[1]->AddVertex(vertices[1]);
          faces[1]->AddVertex(vertices[5]);
          faces[1]->AddVertex(vertices[4]);
          faces[1]->AddSegment(*segments[0]);
          faces[1]->AddSegment(*segments[4]);
          faces[1]->AddSegment(*segments[6]);
          faces[1]->AddSegment(*segments[5]);
          faces[2]->AddVertex(vertices[1]);
          faces[2]->AddVertex(vertices[8]);
          faces[2]->AddVertex(vertices[5]);
          faces[2]->AddSegment(*segments[8]);
          faces[2]->AddSegment(*segments[7]);
          faces[2]->AddSegment(*segments[4]);
          faces[3]->AddVertex(vertices[11]);
          faces[3]->AddVertex(vertices[7]);
          faces[3]->AddVertex(vertices[10]);
          faces[3]->AddSegment(*segments[10]);
          faces[3]->AddSegment(*segments[11]);
          faces[3]->AddSegment(*segments[9]);
          faces[4]->AddVertex(vertices[8]);
          faces[4]->AddVertex(vertices[7]);
          faces[4]->AddVertex(vertices[9]);
          faces[4]->AddSegment(*segments[14]);
          faces[4]->AddSegment(*segments[13]);
          faces[4]->AddSegment(*segments[12]);
          faces[5]->AddVertex(vertices[8]);
          faces[5]->AddVertex(vertices[3]);
          faces[5]->AddVertex(vertices[7]);
          faces[5]->AddSegment(*segments[15]);
          faces[5]->AddSegment(*segments[22]);
          faces[5]->AddSegment(*segments[14]);
          faces[6]->AddVertex(vertices[2]);
          faces[6]->AddVertex(vertices[6]);
          faces[6]->AddVertex(vertices[10]);
          faces[6]->AddVertex(vertices[7]);
          faces[6]->AddVertex(vertices[3]);
          faces[6]->AddSegment(*segments[21]);
          faces[6]->AddSegment(*segments[18]);
          faces[6]->AddSegment(*segments[11]);
          faces[6]->AddSegment(*segments[22]);
          faces[6]->AddSegment(*segments[3]);
          faces[7]->AddVertex(vertices[4]);
          faces[7]->AddVertex(vertices[11]);
          faces[7]->AddVertex(vertices[10]);
          faces[7]->AddVertex(vertices[6]);
          faces[7]->AddSegment(*segments[16]);
          faces[7]->AddSegment(*segments[9]);
          faces[7]->AddSegment(*segments[18]);
          faces[7]->AddSegment(*segments[17]);
          faces[8]->AddVertex(vertices[5]);
          faces[8]->AddVertex(vertices[8]);
          faces[8]->AddVertex(vertices[9]);
          faces[8]->AddVertex(vertices[11]);
          faces[8]->AddSegment(*segments[7]);
          faces[8]->AddSegment(*segments[12]);
          faces[8]->AddSegment(*segments[20]);
          faces[8]->AddSegment(*segments[19]);
          faces[9]->AddVertex(vertices[0]);
          faces[9]->AddVertex(vertices[4]);
          faces[9]->AddVertex(vertices[6]);
          faces[9]->AddVertex(vertices[2]);
          faces[9]->AddSegment(*segments[5]);
          faces[9]->AddSegment(*segments[17]);
          faces[9]->AddSegment(*segments[21]);
          faces[9]->AddSegment(*segments[2]);
          faces[10]->AddVertex(vertices[9]);
          faces[10]->AddVertex(vertices[7]);
          faces[10]->AddVertex(vertices[11]);
          faces[10]->AddSegment(*segments[13]);
          faces[10]->AddSegment(*segments[10]);
          faces[10]->AddSegment(*segments[20]);

          for (unsigned int f = 0; f < numFaces; f++)
              concavePolyhedron.AddFace(*faces[f]);



//Poliedro 2

unsigned int numVertices = 20, numSegments = 33, numFaces = 15;

          concavePolyhedron.Initialize(numVertices, numSegments, numFaces);

          vertices.reserve(numVertices);
          vertices.push_back(Vertex(-0.43442, -4.60756, 0.61722)); // 0
          vertices.push_back(Vertex(3.44871, -4.60756, 0.61722)); // 1
          vertices.push_back(Vertex(3.44871, 0.62709, 0.61722)); // 2
          vertices.push_back(Vertex(-0.43442, 0.62709, 0.61722)); // 3
          vertices.push_back(Vertex(-0.43442, -4.60756, 2.1573)); // 4
          vertices.push_back(Vertex(-0.43442, 0.62709, 2.1573)); // 5
          vertices.push_back(Vertex(2.29482, -4.60756, 2.1573)); // 6
          vertices.push_back(Vertex(2.29482, 0.62709, 2.1573)); // 7
          vertices.push_back(Vertex(2.29482, -4.60756, 9.09767)); // 8
          vertices.push_back(Vertex(2.29482, 0.62709, 9.09767)); // 9
          vertices.push_back(Vertex(3.44871, -4.60756, 9.09767)); // 10
          vertices.push_back(Vertex(3.44871, 0.62709, 9.09767)); // 11
          vertices.push_back(Vertex(3.44871, -4.60756, 7.55759)); // 12
          vertices.push_back(Vertex(3.44871, 0.62709, 7.55759)); // 13
          vertices.push_back(Vertex(6.33726, -4.60756, 9.09767)); // 14
          vertices.push_back(Vertex(6.33726, 0.62709, 7.55759)); // 15
          vertices.push_back(Vertex(6.33726, -4.60756, 7.55759)); // 16
          vertices.push_back(Vertex(6.33726, 0.62709, 9.09767)); // 17
          vertices.push_back(Vertex(2.29482, -4.60756, 0.61722)); // 18
          vertices.push_back(Vertex(2.29482, 0.62709, 0.61722)); // 19

          for (unsigned int s = 0; s < numVertices; s++)
              concavePolyhedron.AddVertex(vertices[s]);

          segments.reserve(numSegments);
          for (unsigned int v = 0; v < numSegments; v++)
              segments.push_back(new Segment(v+1));

          segments[0]->Initialize(vertices[0], vertices[3]);
          segments[1]->Initialize(vertices[3], vertices[5]);
          segments[2]->Initialize(vertices[4], vertices[0]);
          segments[3]->Initialize(vertices[4], vertices[5]);
          segments[4]->Initialize(vertices[5], vertices[7]);
          segments[5]->Initialize(vertices[6], vertices[4]);
          segments[6]->Initialize(vertices[7], vertices[6]);
          segments[7]->Initialize(vertices[6], vertices[18]);
          segments[8]->Initialize(vertices[18], vertices[0]);
          segments[9]->Initialize(vertices[10], vertices[8]);
          segments[10]->Initialize(vertices[18], vertices[1]);
          segments[11]->Initialize(vertices[10], vertices[14]);
          segments[12]->Initialize(vertices[12], vertices[10]);
          segments[13]->Initialize(vertices[14], vertices[16]);
          segments[14]->Initialize(vertices[16], vertices[12]);
          segments[15]->Initialize(vertices[14], vertices[17]);
          segments[16]->Initialize(vertices[15], vertices[16]);
          segments[17]->Initialize(vertices[15], vertices[17]);
          segments[18]->Initialize(vertices[1], vertices[2]);
          segments[19]->Initialize(vertices[2], vertices[13]);
          segments[20]->Initialize(vertices[1], vertices[12]);
          segments[21]->Initialize(vertices[12], vertices[13]);
          segments[22]->Initialize(vertices[8], vertices[9]);
          segments[23]->Initialize(vertices[10], vertices[11]);
          segments[24]->Initialize(vertices[11], vertices[9]);
          segments[25]->Initialize(vertices[17], vertices[11]);
          segments[26]->Initialize(vertices[13], vertices[11]);
          segments[27]->Initialize(vertices[15], vertices[13]);
          segments[28]->Initialize(vertices[7], vertices[19]);
          segments[29]->Initialize(vertices[19], vertices[3]);
          segments[30]->Initialize(vertices[7], vertices[9]);
          segments[31]->Initialize(vertices[8], vertices[6]);
          segments[32]->Initialize(vertices[19], vertices[2]);
          

          for (unsigned int s = 0; s < numSegments; s++)
              concavePolyhedron.AddSegment(*segments[s]);

          faces.reserve(numFaces);
          for (unsigned int f = 0; f < numFaces; f++)
          {
              faces.push_back(new Polygon(f + 1));
              IPolygon& face = *faces[f];

              face.Initialize(3, 3);
          }

          faces[0]->AddVertex(vertices[2]);
          faces[0]->AddVertex(vertices[13]);
          faces[0]->AddVertex(vertices[12]);
          faces[0]->AddVertex(vertices[1]);
          faces[0]->AddSegment(*segments[19]);
          faces[0]->AddSegment(*segments[21]);
          faces[0]->AddSegment(*segments[20]);
          faces[0]->AddSegment(*segments[18]);
          faces[1]->AddVertex(vertices[6]);
          faces[1]->AddVertex(vertices[8]);
          faces[1]->AddVertex(vertices[9]);
          faces[1]->AddVertex(vertices[7]);
          faces[1]->AddSegment(*segments[31]);
          faces[1]->AddSegment(*segments[22]);
          faces[1]->AddSegment(*segments[30]);
          faces[1]->AddSegment(*segments[6]);
          faces[2]->AddVertex(vertices[6]);
          faces[2]->AddVertex(vertices[7]);
          faces[2]->AddVertex(vertices[5]);
          faces[2]->AddVertex(vertices[4]);
          faces[2]->AddSegment(*segments[6]);
          faces[2]->AddSegment(*segments[4]);
          faces[2]->AddSegment(*segments[3]);
          faces[2]->AddSegment(*segments[5]);
          faces[3]->AddVertex(vertices[4]);
          faces[3]->AddVertex(vertices[5]);
          faces[3]->AddVertex(vertices[3]);
          faces[3]->AddVertex(vertices[0]);
          faces[3]->AddSegment(*segments[3]);
          faces[3]->AddSegment(*segments[1]);
          faces[3]->AddSegment(*segments[0]);
          faces[3]->AddSegment(*segments[2]);
          faces[4]->AddVertex(vertices[6]);
          faces[4]->AddVertex(vertices[4]);
          faces[4]->AddVertex(vertices[0]);
          faces[4]->AddVertex(vertices[18]);
          faces[4]->AddSegment(*segments[5]);
          faces[4]->AddSegment(*segments[2]);
          faces[4]->AddSegment(*segments[8]);
          faces[4]->AddSegment(*segments[7]);
          faces[5]->AddVertex(vertices[10]);
          faces[5]->AddVertex(vertices[8]);
          faces[5]->AddVertex(vertices[6]);
          faces[5]->AddVertex(vertices[18]);
          faces[5]->AddVertex(vertices[1]);
          faces[5]->AddVertex(vertices[12]);
          faces[5]->AddSegment(*segments[9]);
          faces[5]->AddSegment(*segments[31]);
          faces[5]->AddSegment(*segments[7]);
          faces[5]->AddSegment(*segments[10]);
          faces[5]->AddSegment(*segments[20]);
          faces[5]->AddSegment(*segments[12]);
          faces[6]->AddVertex(vertices[14]);
          faces[6]->AddVertex(vertices[10]);
          faces[6]->AddVertex(vertices[12]);
          faces[6]->AddVertex(vertices[16]);
          faces[6]->AddSegment(*segments[11]);
          faces[6]->AddSegment(*segments[12]);
          faces[6]->AddSegment(*segments[14]);
          faces[6]->AddSegment(*segments[13]);
          faces[7]->AddVertex(vertices[17]);
          faces[7]->AddVertex(vertices[14]);
          faces[7]->AddVertex(vertices[16]);
          faces[7]->AddVertex(vertices[15]);
          faces[7]->AddSegment(*segments[15]);
          faces[7]->AddSegment(*segments[13]);
          faces[7]->AddSegment(*segments[16]);
          faces[7]->AddSegment(*segments[17]);
          faces[8]->AddVertex(vertices[15]);
          faces[8]->AddVertex(vertices[16]);
          faces[8]->AddVertex(vertices[12]);
          faces[8]->AddVertex(vertices[13]);
          faces[8]->AddSegment(*segments[16]);
          faces[8]->AddSegment(*segments[14]);
          faces[8]->AddSegment(*segments[22]);
          faces[8]->AddSegment(*segments[27]);
          faces[9]->AddVertex(vertices[11]);
          faces[9]->AddVertex(vertices[17]);
          faces[9]->AddVertex(vertices[15]);
          faces[9]->AddVertex(vertices[13]);
          faces[9]->AddSegment(*segments[25]);
          faces[9]->AddSegment(*segments[17]);
          faces[9]->AddSegment(*segments[27]);
          faces[9]->AddSegment(*segments[26]);
          faces[10]->AddVertex(vertices[9]);
          faces[10]->AddVertex(vertices[11]);
          faces[10]->AddVertex(vertices[13]);
          faces[10]->AddVertex(vertices[2]);
          faces[10]->AddVertex(vertices[19]);
          faces[10]->AddVertex(vertices[7]);
          faces[10]->AddSegment(*segments[24]);
          faces[10]->AddSegment(*segments[26]);
          faces[10]->AddSegment(*segments[19]);
          faces[10]->AddSegment(*segments[32]);
          faces[10]->AddSegment(*segments[28]);
          faces[10]->AddSegment(*segments[30]);
          faces[11]->AddVertex(vertices[5]);
          faces[11]->AddVertex(vertices[7]);
          faces[11]->AddVertex(vertices[19]);
          faces[11]->AddVertex(vertices[3]);
          faces[11]->AddSegment(*segments[4]);
          faces[11]->AddSegment(*segments[28]);
          faces[11]->AddSegment(*segments[29]);
          faces[11]->AddSegment(*segments[1]);
          faces[12]->AddVertex(vertices[8]);
          faces[12]->AddVertex(vertices[10]);
          faces[12]->AddVertex(vertices[11]);
          faces[12]->AddVertex(vertices[9]);
          faces[12]->AddSegment(*segments[9]);
          faces[12]->AddSegment(*segments[23]);
          faces[12]->AddSegment(*segments[24]);
          faces[12]->AddSegment(*segments[22]);
          faces[13]->AddVertex(vertices[3]);
          faces[13]->AddVertex(vertices[19]);
          faces[13]->AddVertex(vertices[2]);
          faces[13]->AddVertex(vertices[1]);
          faces[13]->AddVertex(vertices[18]);
          faces[13]->AddVertex(vertices[0]);
          faces[13]->AddSegment(*segments[29]);
          faces[13]->AddSegment(*segments[32]);
          faces[13]->AddSegment(*segments[18]);
          faces[13]->AddSegment(*segments[10]);
          faces[13]->AddSegment(*segments[8]);
          faces[13]->AddSegment(*segments[0]);
          faces[14]->AddVertex(vertices[14]);
          faces[14]->AddVertex(vertices[17]);
          faces[14]->AddVertex(vertices[11]);
          faces[14]->AddVertex(vertices[10]);
          faces[14]->AddSegment(*segments[15]);
          faces[14]->AddSegment(*segments[25]);
          faces[14]->AddSegment(*segments[23]);
          faces[14]->AddSegment(*segments[11]);

          for (unsigned int f = 0; f < numFaces; f++)
              concavePolyhedron.AddFace(*faces[f]);