#include <Segment.hpp>
#include <Polygon.hpp>
#include "Eigen"
#include "Input.hpp"
#include "EigenExtension.hpp"
#include "Polyhedron.hpp"
#include "geometryUnitTest.hpp"
#include "UnitTestSummary.hpp"

using namespace std;
using namespace Eigen;
using namespace GeDiM;
using namespace UnitTest;

int main(int argc, char **argv)
{
   /* unsigned int numVertices = 8, numSegments = 12, numFaces = 6;
    auto poliedro = new Polyhedron();
    poliedro->Initialize(numVertices, numSegments, numFaces);

    vector<Vertex> vertices;
    vertices.reserve(numVertices);

    vertices.push_back(Vertex(1.0000e+00, 1.0000e+00, 1.0000e+00)); // 0
    vertices.push_back(Vertex(2.0000e+00, 1.0000e+00, 1.0000e+00)); // 1
    vertices.push_back(Vertex(2.0000e+00, 2.0000e+00, 1.0000e+00)); // 2
    vertices.push_back(Vertex(1.0000e+00, 2.0000e+00, 1.0000e+00)); // 3
    vertices.push_back(Vertex(1.0000e+00, 1.0000e+00, 2.0000e+00)); // 4
    vertices.push_back(Vertex(2.0000e+00, 1.0000e+00, 2.0000e+00)); // 5
    vertices.push_back(Vertex(2.0000e+00, 2.0000e+00, 2.0000e+00)); // 6
    vertices.push_back(Vertex(1.0000e+00, 2.0000e+00, 2.0000e+00)); // 7

    for (unsigned int s = 0; s < numVertices; s++)
        poliedro->AddVertex(vertices[s]);

    vector<Segment> segments;
    segments.resize(numSegments);

    segments[0].Initialize(vertices[0], vertices[1]);
    segments[1].Initialize(vertices[1], vertices[2]);
    segments[2].Initialize(vertices[2], vertices[3]);
    segments[3].Initialize(vertices[3], vertices[0]);
    segments[4].Initialize(vertices[0], vertices[4]);
    segments[5].Initialize(vertices[1], vertices[5]);
    segments[6].Initialize(vertices[2], vertices[6]);
    segments[7].Initialize(vertices[3], vertices[7]);
    segments[8].Initialize(vertices[4], vertices[5]);
    segments[9].Initialize(vertices[5], vertices[6]);
    segments[10].Initialize(vertices[6], vertices[7]);
    segments[11].Initialize(vertices[7], vertices[4]);

    for (unsigned int s = 0; s < numSegments; s++)
        poliedro->AddSegment(segments[s]);

    vector<Polygon> faces;
    faces.resize(numFaces);

    for (unsigned int f = 0; f < numFaces; f++)
        faces[f].Initialize(4, 4);

    faces[0].AddVertex(vertices[0]);
    faces[0].AddVertex(vertices[1]);
    faces[0].AddVertex(vertices[2]);
    faces[0].AddVertex(vertices[3]);
    faces[0].AddSegment(segments[0]);
    faces[0].AddSegment(segments[1]);
    faces[0].AddSegment(segments[2]);
    faces[0].AddSegment(segments[3]);

    faces[1].AddVertex(vertices[2]);
    faces[1].AddVertex(vertices[6]);
    faces[1].AddVertex(vertices[7]);
    faces[1].AddVertex(vertices[3]);
    faces[1].AddSegment(segments[6]);
    faces[1].AddSegment(segments[10]);
    faces[1].AddSegment(segments[7]);
    faces[1].AddSegment(segments[2]);

    faces[2].AddVertex(vertices[3]);
    faces[2].AddVertex(vertices[7]);
    faces[2].AddVertex(vertices[4]);
    faces[2].AddVertex(vertices[0]);
    faces[2].AddSegment(segments[7]);
    faces[2].AddSegment(segments[11]);
    faces[2].AddSegment(segments[4]);
    faces[2].AddSegment(segments[3]);

    faces[3].AddVertex(vertices[0]);
    faces[3].AddVertex(vertices[4]);
    faces[3].AddVertex(vertices[5]);
    faces[3].AddVertex(vertices[1]);
    faces[3].AddSegment(segments[3]);
    faces[3].AddSegment(segments[8]);
    faces[3].AddSegment(segments[5]);
    faces[3].AddSegment(segments[0]);

    faces[4].AddVertex(vertices[1]);
    faces[4].AddVertex(vertices[5]);
    faces[4].AddVertex(vertices[6]);
    faces[4].AddVertex(vertices[2]);
    faces[4].AddSegment(segments[5]);
    faces[4].AddSegment(segments[9]);
    faces[4].AddSegment(segments[6]);
    faces[4].AddSegment(segments[1]);

    faces[5].AddVertex(vertices[4]);
    faces[5].AddVertex(vertices[5]);
    faces[5].AddVertex(vertices[6]);
    faces[5].AddVertex(vertices[7]);
    faces[5].AddSegment(segments[8]);
    faces[5].AddSegment(segments[9]);
    faces[5].AddSegment(segments[10]);
    faces[5].AddSegment(segments[11]);

    for (unsigned int f = 0; f < numFaces; f++)
        poliedro->AddFace(faces[f]);

    const Polygon& faccia_cambiata = dynamic_cast<const Polygon &>(poliedro->GetFace(5));
    auto faccia_anl = const_cast<Polygon &>(faccia_cambiata);
    faccia_anl.ComputePlane();
    Vector3d normale;
    normale = faccia_anl.Normal();
    printf("%lf, %lf, %lf", normale[0], normale[1], normale[2]);*/




  /// Set Variables
  double tolerance = 1e-14;
  Input::AddProperty("tolerance", tolerance);
  Input::Initialize(argc, argv);
  Input::GetPropertyValue("tolerance", tolerance);

  /// Run Unit Tests
  Geometry::UnitTest::RunPolyhedronTests(tolerance);
  Output::PrintLine('*');

  /// Print Summary
  UnitTestSummary::PrintSummary();
  Output::PrintLine('*');
}
